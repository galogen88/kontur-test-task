package task.kontur.main

import android.os.Handler
import android.os.Looper
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor

fun Disposable.storeToComposite(composite: CompositeDisposable) {
    composite.add(this)
}

class UIScheduler {

    companion object {
        private val executor = UIThreadExecutor()

        @JvmStatic
        fun uiThread(): Scheduler {
            return Schedulers.from(executor)
        }
    }

    private class UIThreadExecutor : Executor {

        private val handler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable?) {
            if (command != null) {
                if (Thread.currentThread() === Looper.getMainLooper().thread) {
                    command.run()
                } else {
                    handler.post(command)
                }
            }
        }
    }
}