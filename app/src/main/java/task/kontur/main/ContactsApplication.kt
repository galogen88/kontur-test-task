package task.kontur.main

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import task.kontur.main.dagger.DaggerAppComponent

class ContactsApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
            .application(this)
            .build()
    }
}