package task.kontur.main.dagger

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import task.kontur.contacts.dagger.ContactsModule
import task.kontur.main.data.NetworkConnectivityReceiver
import task.kontur.main.domain.INetworkConnectivityReceiver
import task.kontur.main.presentation.MainActivityModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApplicationModule::class,
        MainActivityModule::class,
        ContactsModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}

@Module
object ApplicationModule {
    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideConnectivityUseCases(
        userCases: NetworkConnectivityReceiver
    ): INetworkConnectivityReceiver = userCases

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("contacts_preferences", Context.MODE_PRIVATE)
}