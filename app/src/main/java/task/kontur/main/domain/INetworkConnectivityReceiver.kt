package task.kontur.main.domain

import io.reactivex.Flowable

interface INetworkConnectivityReceiver {
    fun isOnlineFlow(): Flowable<Boolean>
}