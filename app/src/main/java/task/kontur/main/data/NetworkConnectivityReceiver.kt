package task.kontur.main.data

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import task.kontur.main.domain.INetworkConnectivityReceiver
import javax.inject.Inject

class NetworkConnectivityReceiver @Inject constructor(
    context: Context
) : BroadcastReceiver(), INetworkConnectivityReceiver {
    companion object {
        private const val CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE"
    }

    private val onlineProcessor = BehaviorProcessor.create<Boolean>()

    init {
        val intentFilter = IntentFilter()
        intentFilter.addAction(CONNECTIVITY_ACTION)
        context.registerReceiver(this, intentFilter)
        onlineProcessor.onNext(isOnline(context))
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let {
            onlineProcessor.onNext(isOnline(context))
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        return networkInfo?.isConnected == true
    }

    override fun isOnlineFlow(): Flowable<Boolean> = onlineProcessor
}