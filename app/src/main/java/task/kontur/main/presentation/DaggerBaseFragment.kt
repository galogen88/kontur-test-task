package task.kontur.main.presentation

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class DaggerBaseFragment<V : Any> : Fragment(), HasAndroidInjector {
    protected val onStartSubscription: CompositeDisposable = CompositeDisposable()
    protected val onCreateSubscription: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var viewModel: V

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onDestroy() {
        super.onDestroy()
        onCreateSubscription.clear()
    }

    override fun onStop() {
        super.onStop()
        onStartSubscription.clear()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}