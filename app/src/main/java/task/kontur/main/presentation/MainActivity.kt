package task.kontur.main.presentation

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjection
import dagger.android.ContributesAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.Disposable
import task.kontur.R
import task.kontur.contacts.dagger.ContactsUIModule
import task.kontur.main.UIScheduler
import javax.inject.Inject

@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [MainActivityViewModelModule::class, ContactsUIModule::class])
    abstract fun contributeMainActivity(): MainActivity
}

@Module
object MainActivityViewModelModule {
    @Provides
    fun provideMainViewModel(
        activity: MainActivity
    ): IMainViewModel {
        return provideViewModelImpl(activity)
    }

    @Provides
    fun provideCommonNavigator(
        activity: MainActivity
    ): ICommonNavigator {
        return provideViewModelImpl(activity)
    }

    private fun provideViewModelImpl(activity: MainActivity) =
        ViewModelProviders.of(activity)[MainViewModelImpl::class.java]

}

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModel: IMainViewModel

    private lateinit var navigationDisposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        navigationDisposable = viewModel.getNavigationCommandsFlow()
            .observeOn(UIScheduler.uiThread())
            .subscribe { navigate(it) }
    }

    private fun navigate(command: NavigationCommand) {
        val navController = findNavController(R.id.nav_host_fragment)
        when (command) {
            is NavigationCommand.To -> navController.navigate(command.directions)
            NavigationCommand.Back -> navController.navigateUp()
            NavigationCommand.ToRoot -> navController.popBackStack(R.id.contactsListFragment, false)
            is NavigationCommand.ToIntent -> {
                command.intent.resolveActivity(packageManager)?.let {
                    startActivity(command.intent)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        navigationDisposable.dispose()
    }
}
