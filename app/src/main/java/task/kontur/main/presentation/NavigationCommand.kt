package task.kontur.main.presentation

import android.content.Intent
import androidx.navigation.NavDirections

sealed class NavigationCommand {
    data class To(val directions: NavDirections) : NavigationCommand()
    object Back : NavigationCommand()
    object ToRoot : NavigationCommand()
    data class ToIntent(val intent: Intent) : NavigationCommand()
}