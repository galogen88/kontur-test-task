package task.kontur.main.presentation

interface ICommonNavigator {
    fun navigate(command: NavigationCommand)
}