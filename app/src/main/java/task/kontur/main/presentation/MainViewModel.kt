package task.kontur.main.presentation

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor

interface IMainViewModel : ICommonNavigator {
    fun getNavigationCommandsFlow(): Flowable<NavigationCommand>
}

class MainViewModelImpl : ViewModel(), IMainViewModel {
    private val navigationCommandsProcessor = PublishProcessor.create<NavigationCommand>()

    override fun getNavigationCommandsFlow(): Flowable<NavigationCommand> {
        return navigationCommandsProcessor
    }

    override fun navigate(command: NavigationCommand) {
        navigationCommandsProcessor.onNext(command)
    }

    override fun onCleared() {
        super.onCleared()
        navigationCommandsProcessor.onComplete()
    }
}