package task.kontur.contacts.data

import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.processors.BehaviorProcessor
import task.kontur.contacts.domain.Contact
import task.kontur.contacts.domain.IContactsLocalDataSource
import task.kontur.contacts.domain.IContactsServerDataSource
import task.kontur.contacts.domain.RequestStatus
import task.kontur.contacts.presentation.IContactsRepository
import javax.inject.Inject

class ContactsRepositoryImpl @Inject constructor(
    private val serverDataSource: IContactsServerDataSource,
    private val localDataSource: IContactsLocalDataSource
) : IContactsRepository {
    private val filterProcessor = BehaviorProcessor.create<String>()
    private val requestStatusProcessor = BehaviorProcessor.create<RequestStatus>()

    override fun storeFilter(filter: String) {
        filterProcessor.onNext(filter)
    }

    override fun getContactFlow(contactId: String): Flowable<Contact> {
        return localDataSource.getContactFlow(contactId)
    }

    override fun getContactsByPhoneNumber(phoneNumberFilter: String): Flowable<List<Contact>> {
        return localDataSource.getContactsByPhone(phoneNumberFilter)
    }

    override fun getFilterFlow(): Flowable<String> = filterProcessor

    override fun getContactsFlow(filter: String): Flowable<List<Contact>> =
        localDataSource.getContactsList(filter)

    override fun requestContacts(): Flowable<List<Contact>> {
        return Single
            .concat(
                serverDataSource.loadPortion("01"),
                serverDataSource.loadPortion("02"),
                serverDataSource.loadPortion("03")
            )
    }

    override fun storeRequestStatus(status: RequestStatus) {
        requestStatusProcessor.onNext(status)
    }

    override fun getRequestStatus(): Flowable<RequestStatus> = requestStatusProcessor

    override fun storeContacts(portion: List<Contact>) {
        localDataSource.storeContactsListPortion(portion)
    }

    override fun storeTimestamp(timestamp: Long) {
        localDataSource.storeLastUpdateTimeFlow(timestamp)
    }

    override fun getTimestamp(defaultTimestamp: Long): Long =
        localDataSource.getLastUpdateTime(defaultTimestamp)

}