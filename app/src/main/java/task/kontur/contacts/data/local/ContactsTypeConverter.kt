package task.kontur.contacts.data.local

import androidx.room.TypeConverter
import task.kontur.contacts.domain.Period
import task.kontur.contacts.domain.Temperament
import java.util.*

class ContactsTypeConverter {

    @TypeConverter
    fun temperamentToString(temperament: Temperament): String {
        return temperament.name
    }

    @TypeConverter
    fun stringToTemperament(string: String): Temperament {
        return Temperament.values().first { it.name == string }
    }

    @TypeConverter
    fun periodToString(period: Period): String {
        return period.start.time.toString() + " " + period.end.time.toString()
    }

    @TypeConverter
    fun stringToPeriod(string: String): Period {
        val dates = string.split(" ").map { Date(it.toLong()) }
        return Period(dates[0], dates[1])
    }
}