package task.kontur.contacts.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import task.kontur.contacts.domain.Contact

@Database(entities = [Contact::class], version = 1, exportSchema = false)
abstract class ContactsDataBase : RoomDatabase() {
    abstract fun contactsDao(): ContactsDao
}