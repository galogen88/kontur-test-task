package task.kontur.contacts.data.local

import androidx.room.*
import io.reactivex.Flowable
import task.kontur.contacts.domain.Contact

@Dao
interface ContactsDao {
//    @Query("SELECT * FROM contacts")
//    fun getContactsListFlow(): Flowable<List<Contact>>

    @Query("SELECT * FROM contacts WHERE id = :id LIMIT 1")
    fun getContactFlow(id: String): Flowable<Contact>

    @Query("SELECT * FROM contacts WHERE name LIKE :filter OR phone LIKE :filter")
    // should pass filter as string wrapped with %%
    fun getFilteredContactsList(filter: String): Flowable<List<Contact>>

    @Query("SELECT * FROM contacts WHERE phone LIKE :filter")
    // should pass filter as string wrapped with %%
    fun getContactsListByPhoneNumber(filter: String): Flowable<List<Contact>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContactsList(contacts: List<Contact>)
}