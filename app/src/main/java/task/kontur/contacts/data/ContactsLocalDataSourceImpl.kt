package task.kontur.contacts.data

import android.content.SharedPreferences
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import task.kontur.contacts.data.local.ContactsDao
import task.kontur.contacts.domain.Contact
import task.kontur.contacts.domain.IContactsLocalDataSource
import java.util.concurrent.Executors
import javax.inject.Inject

class ContactsLocalDataSourceImpl @Inject constructor(
    private val contactsDao: ContactsDao,
    private val sharedPreferences: SharedPreferences
) : IContactsLocalDataSource {
    companion object {
        private const val TIMESTAMP_KEY = "timestamp"
    }

    private val scheduler = Schedulers.from(Executors.newSingleThreadExecutor())

    override fun getContactFlow(id: String): Flowable<Contact> {
        return contactsDao.getContactFlow(id)
    }

    override fun storeContactsListPortion(portion: List<Contact>) {
        Completable
            .fromAction { contactsDao.insertContactsList(portion) }
            .subscribeOn(scheduler)
            .subscribe()
    }

    override fun getContactsList(filter: String): Flowable<List<Contact>> {
        return contactsDao.getFilteredContactsList("%$filter%")
    }

    override fun getContactsByPhone(phoneNumberFilter: String): Flowable<List<Contact>> {
        return contactsDao.getContactsListByPhoneNumber("%$phoneNumberFilter%")
    }

    override fun getLastUpdateTime(defaultTimestamp: Long): Long {
        return sharedPreferences.getLong(TIMESTAMP_KEY, defaultTimestamp)
    }

    override fun storeLastUpdateTimeFlow(time: Long) {
        sharedPreferences.edit()
            .putLong(TIMESTAMP_KEY, time)
            .apply()
    }
}