package task.kontur.contacts.data.remote

import io.reactivex.Single
import task.kontur.contacts.domain.Contact
import task.kontur.contacts.domain.IContactsServerDataSource
import javax.inject.Inject

class ContactsServerDataSourceImpl @Inject constructor(
    private val api: ContactsApi
) : IContactsServerDataSource {
    override fun loadPortion(id: String): Single<List<Contact>> {
        return api.loadPortion(id)
    }
}