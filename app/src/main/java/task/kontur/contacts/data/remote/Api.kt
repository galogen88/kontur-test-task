package task.kontur.contacts.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import task.kontur.contacts.domain.Contact

interface ContactsApi {
    @GET("SkbkonturMobile/mobile-test-droid/master/json/generated-{id}.json")
    fun loadPortion(@Path("id") id: String): Single<List<Contact>>
}

const val BASE_URL = "https://raw.githubusercontent.com"