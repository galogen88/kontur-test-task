package task.kontur.contacts.presentation

import io.reactivex.Flowable
import task.kontur.contacts.domain.Contact
import task.kontur.contacts.domain.RequestStatus

interface IContactsRepository {
    fun getContactFlow(contactId: String): Flowable<Contact>

    fun getContactsFlow(filter: String = ""): Flowable<List<Contact>>
    fun getContactsByPhoneNumber(phoneNumberFilter: String): Flowable<List<Contact>>
    fun requestContacts(): Flowable<List<Contact>>
    fun storeContacts(portion: List<Contact>)

    fun storeRequestStatus(status: RequestStatus)
    fun getRequestStatus(): Flowable<RequestStatus>

    fun storeFilter(filter: String)
    fun getFilterFlow(): Flowable<String>

    fun storeTimestamp(timestamp: Long)
    fun getTimestamp(defaultTimestamp: Long): Long
}