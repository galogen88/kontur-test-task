package task.kontur.contacts.presentation

data class ContactItem(
    val id: String,
    val name: String,
    val phone: String,
    val height: String
)

data class ContactsViewState(
    val progressVisible: Boolean,
    val swipeProgressVisible: Boolean,
    val emptyListStubVisible: Boolean,
    val onlineStatusWarningVisible: Boolean
)

data class ContactInfoViewState(
    val name : String,
    val phoneNumber : String,
    val temperament: String,
    val period: String,
    val bio: String
)