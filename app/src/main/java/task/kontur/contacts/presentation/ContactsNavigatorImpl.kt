package task.kontur.contacts.presentation

import android.content.Intent
import android.net.Uri
import task.kontur.main.presentation.ICommonNavigator
import task.kontur.main.presentation.NavigationCommand
import javax.inject.Inject

class ContactsNavigatorImpl @Inject constructor(
    private val commonNavigator: ICommonNavigator
    ) : IContactsNavigator {
    override fun back() {
        commonNavigator.navigate(NavigationCommand.Back)
    }

    override fun openContact(contactId: String) {
        commonNavigator.navigate(
            NavigationCommand.To(
                ContactsListFragmentDirections.openContact(
                    contactId
                )
            )
        )
    }

    override fun callTo(phone: String) {
        val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
        commonNavigator.navigate(NavigationCommand.ToIntent(callIntent))
    }
}