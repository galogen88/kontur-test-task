package task.kontur.contacts.presentation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.contacts_list_fragment.*
import kotlinx.android.synthetic.main.search_action_bar_layout.*
import task.kontur.R
import task.kontur.main.UIScheduler
import task.kontur.main.presentation.DaggerBaseFragment
import task.kontur.main.storeToComposite

class ContactsListFragment : DaggerBaseFragment<IContactsViewModel>() {

    companion object {
        fun create() = ContactsListFragment()
    }

    private lateinit var connectionWarningSnackbar: Snackbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.contacts_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        connectionWarningSnackbar =
            Snackbar.make(
                root,
                getString(R.string.warning_is_offline),
                Snackbar.LENGTH_INDEFINITE
            )

        clear_action.setOnClickListener { search_filter.setText("") }
        search_filter.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                editable?.toString()?.let { viewModel.onSearchFilterTextChanged(it) }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })


        val adapter = ContactsAdapter { viewModel.onContactClick(it) }
        viewModel.getContactsFlow()
            .observeOn(UIScheduler.uiThread())
            .subscribe { adapter.submitList(it) }
            .storeToComposite(onCreateSubscription)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        recycler_wrapper.setOnRefreshListener { viewModel.onSwipeToUpUpdate() }
    }

    override fun onStart() {
        super.onStart()

        viewModel.getContactsViewStateFlow()
            .observeOn(UIScheduler.uiThread())
            .subscribe { state ->
                empty_data_stub.visibility = if (state.emptyListStubVisible) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }

                recycler_wrapper.isRefreshing = state.swipeProgressVisible

                if (state.progressVisible) {
                    progress.show()
                } else {
                    progress.hide()
                }

                if (state.onlineStatusWarningVisible) {
                    connectionWarningSnackbar.show()
                } else {
                    connectionWarningSnackbar.dismiss()
                }
            }
            .storeToComposite(onStartSubscription)
    }
}