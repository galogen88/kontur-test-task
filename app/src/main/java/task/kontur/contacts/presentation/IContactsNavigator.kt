package task.kontur.contacts.presentation

interface IContactsNavigator {
    fun back()
    fun openContact(contactId: String)
    fun callTo(phone: String)
}

