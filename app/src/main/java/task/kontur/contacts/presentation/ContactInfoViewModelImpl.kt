package task.kontur.contacts.presentation

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import task.kontur.contacts.domain.IContactsUseCases
import task.kontur.contacts.domain.ITextUseCases
import task.kontur.main.UIScheduler
import javax.inject.Inject
import javax.inject.Named

interface IContactInfoViewModel {
    fun getContactInfoViewState(): Flowable<ContactInfoViewState>
    fun onBackArrowPressed()
    fun callByPhone()
}

class ContactInfoViewModelImpl @Inject constructor(
    private val contactUseCases: IContactsUseCases,
    @Named("contactId")
    private val contactId: String,
    private val textUseCases: ITextUseCases,
    private val navigator: IContactsNavigator
) : IContactInfoViewModel, ViewModel() {
    private var callDisposable: Disposable = Disposables.disposed()

    override fun getContactInfoViewState(): Flowable<ContactInfoViewState> {
        return contactUseCases.getContactInfo(contactId)
            .subscribeOn(Schedulers.io())
            .map {
                ContactInfoViewState(
                    name = it.name,
                    phoneNumber = textUseCases.formatSimplePhoneNumber(it.phone),
                    temperament = textUseCases.localizeTemperament(it.temperament),
                    period = textUseCases.localizePeriod(it.educationPeriod),
                    bio = it.biography
                )
            }
    }

    override fun onBackArrowPressed() {
        navigator.back()
    }

    override fun callByPhone() {
        if (callDisposable.isDisposed) {
            callDisposable = contactUseCases.getContactInfo(contactId)
                .firstElement()
                .subscribeOn(Schedulers.io())
                .observeOn(UIScheduler.uiThread())
                .subscribe {
                    navigator.callTo(textUseCases.formatSimplePhoneNumber(it.phone))
                }
        }
    }

    override fun onCleared() {
        callDisposable.dispose()

    }
}