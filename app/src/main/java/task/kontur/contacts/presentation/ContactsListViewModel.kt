package task.kontur.contacts.presentation

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.functions.Function3
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers
import task.kontur.contacts.domain.IContactsUseCases
import task.kontur.contacts.domain.ITextUseCases
import task.kontur.contacts.domain.RequestStatus
import task.kontur.main.domain.INetworkConnectivityReceiver
import javax.inject.Inject

interface IContactsViewModel {
    fun getContactsFlow(): Flowable<List<ContactItem>>
    fun onContactClick(contact: ContactItem)
    fun onSearchFilterTextChanged(text: String)
    fun onSwipeToUpUpdate()
    fun getContactsViewStateFlow(): Flowable<ContactsViewState>
}

class ContactsViewModelImpl @Inject constructor(
    private val networkConnectivityReceiver: INetworkConnectivityReceiver,
    private val contactsUseCases: IContactsUseCases,
    private val navigator: IContactsNavigator,
    private val textUseCases: ITextUseCases
) : IContactsViewModel, ViewModel() {
    private var showSwipeToRefresh = false
    private val listProcessor = BehaviorProcessor.create<List<ContactItem>>()
    private val dataChangeDisposable = contactsUseCases.getContactsListFlow()
        .subscribeOn(Schedulers.io())
        .map { list ->
            list.map { contact ->
                ContactItem(
                    id = contact.id,
                    name = contact.name,
                    phone = textUseCases.formatSimplePhoneNumber(contact.phone),
                    height = contact.height.toString()
                )
            }
        }
        .subscribe { listProcessor.onNext(it) }

    override fun onSearchFilterTextChanged(text: String) {
        contactsUseCases.setFilter(text)
    }

    override fun onContactClick(contact: ContactItem) {
        navigator.openContact(contact.id)
    }

    override fun onSwipeToUpUpdate() {
        showSwipeToRefresh = true
        return contactsUseCases.updateContactsList()
    }

    override fun getContactsFlow(): Flowable<List<ContactItem>> {
        return listProcessor
    }

    override fun getContactsViewStateFlow(): Flowable<ContactsViewState> {
        return Flowable
            .combineLatest(
                listProcessor.map { it.isEmpty() },
                contactsUseCases.loadingStatus(),
                networkConnectivityReceiver.isOnlineFlow(),
                Function3 { emptyData: Boolean, requestStatus: RequestStatus, isOnline: Boolean ->
                    val swipeRefreshVisible =
                        showSwipeToRefresh && requestStatus == RequestStatus.IN_PROGRESS

                    val progressVisible =
                        !showSwipeToRefresh && requestStatus == RequestStatus.IN_PROGRESS && emptyData

                    val stubVisible = emptyData && requestStatus == RequestStatus.SUCCESS

                    val onlineWarningVisible = requestStatus != RequestStatus.SUCCESS && !isOnline

                    if (showSwipeToRefresh) {
                        showSwipeToRefresh = swipeRefreshVisible
                    }

                    ContactsViewState(
                        progressVisible = progressVisible,
                        swipeProgressVisible = swipeRefreshVisible,
                        emptyListStubVisible = stubVisible,
                        onlineStatusWarningVisible = onlineWarningVisible
                    )
                }
            )
            .subscribeOn(Schedulers.io())
    }

    override fun onCleared() {
        dataChangeDisposable.dispose()
    }
}
