package task.kontur.contacts.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.action_bar_layout.*
import kotlinx.android.synthetic.main.contact_info_layout.*
import task.kontur.R
import task.kontur.main.UIScheduler
import task.kontur.main.presentation.DaggerBaseFragment
import task.kontur.main.storeToComposite

class ContactInfoFragment : DaggerBaseFragment<IContactInfoViewModel>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.contact_info_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        back_arrow.setOnClickListener { viewModel.onBackArrowPressed() }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getContactInfoViewState()
            .observeOn(UIScheduler.uiThread())
            .subscribe { state ->
                name.text = state.name
                phone_number.text = state.phoneNumber
                temperament.text = state.temperament
                period.text = state.period
                biography.text = state.bio

                phone_number.setOnClickListener { viewModel.callByPhone() }
            }
            .storeToComposite(onStartSubscription)
    }
}