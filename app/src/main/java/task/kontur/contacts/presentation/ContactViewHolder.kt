package task.kontur.contacts.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import task.kontur.R
import kotlinx.android.synthetic.main.contact_item_layout.view.*

class ContactViewHolder(
    parent: ViewGroup,
    private val clickAction: (ContactItem) -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.contact_item_layout,
        parent,
        false
    )
) {
    fun bind(item: ContactItem) {
        itemView.name.text = item.name
        itemView.phone_number.text = item.phone
        itemView.height_info.text = item.height
        itemView.setOnClickListener { clickAction(item) }
    }

}