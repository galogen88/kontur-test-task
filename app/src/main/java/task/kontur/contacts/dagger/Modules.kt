package task.kontur.contacts.dagger

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import task.kontur.contacts.data.ContactsLocalDataSourceImpl
import task.kontur.contacts.data.ContactsRepositoryImpl
import task.kontur.contacts.data.local.ContactsDao
import task.kontur.contacts.data.local.ContactsDataBase
import task.kontur.contacts.data.remote.BASE_URL
import task.kontur.contacts.data.remote.ContactsApi
import task.kontur.contacts.data.remote.ContactsServerDataSourceImpl
import task.kontur.contacts.domain.*
import task.kontur.contacts.presentation.*
import task.kontur.main.presentation.DaggerViewModelFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
object ContactsModule {
    @Provides
    @Singleton
    fun provideUseCases(userCases: ContactsUseCasesImpl): IContactsUseCases = userCases

    @Provides
    @Singleton
    fun provideRepository(repository: ContactsRepositoryImpl): IContactsRepository = repository

    @Provides
    @Singleton
    fun provideServerDataSource(serverDataSource: ContactsServerDataSourceImpl): IContactsServerDataSource =
        serverDataSource

    @Provides
    @Singleton
    fun providesApi(): ContactsApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ContactsApi::class.java)
    }

    @Provides
    @Singleton
    fun provideLocalDataSource(localDataSource: ContactsLocalDataSourceImpl): IContactsLocalDataSource =
        localDataSource

    @Provides
    @Singleton
    fun provideContactsDao(context: Context): ContactsDao =
        Room
            .databaseBuilder(
                context,
                ContactsDataBase::class.java,
                "kontur-contacts"
            ).build()
            .contactsDao()
}

@Module
object ContactsViewModelsModule {
    @Provides
    fun provideContactsListViewModel(
        fragment: ContactsListFragment,
        factory: DaggerViewModelFactory<ContactsViewModelImpl>
    ): IContactsViewModel {
        return ViewModelProviders.of(fragment, factory).get(ContactsViewModelImpl::class.java)
    }

    @Provides
    @Named("contactId")
    fun provideContactId(fragment: ContactInfoFragment): String =
        fragment.arguments?.let {
            ContactInfoFragmentArgs.fromBundle(it).contactId
        } ?: ""

    @Provides
    fun provideTextUseCases(useCase: TextUseCasesImpl): ITextUseCases =
        useCase

    @Provides
    fun provideContactInfoViewMode(
        fragment: ContactInfoFragment,
        factory: DaggerViewModelFactory<ContactInfoViewModelImpl>
    ): IContactInfoViewModel {
        return ViewModelProviders.of(fragment, factory).get(ContactInfoViewModelImpl::class.java)
    }

    @Provides
    fun provideContactsNavigator(navigator: ContactsNavigatorImpl): IContactsNavigator =
        navigator
}

@Module
abstract class ContactsUIModule {
    @ContributesAndroidInjector(modules = [ContactsViewModelsModule::class])
    abstract fun contributeContactsListFragment(): ContactsListFragment

    @ContributesAndroidInjector(modules = [ContactsViewModelsModule::class])
    abstract fun contributeContactInfoFragment(): ContactInfoFragment
}