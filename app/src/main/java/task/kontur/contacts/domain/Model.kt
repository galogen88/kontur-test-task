package task.kontur.contacts.domain

import androidx.room.*
import com.google.gson.annotations.SerializedName
import task.kontur.contacts.data.local.ContactsTypeConverter
import java.util.*

@Entity(tableName = "contacts")
@TypeConverters(ContactsTypeConverter::class)
data class Contact(
    @PrimaryKey
    @SerializedName("id")
    val id: String,

    @SerializedName("name") val name: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("height") val height: Float,
    @SerializedName("biography") val biography: String,
    @SerializedName("temperament") val temperament: Temperament,

    @SerializedName("educationPeriod")
    val educationPeriod: Period
)

data class Period(
    @SerializedName("start") val start: Date,
    @SerializedName("end") val end: Date
)

enum class Temperament {
    @SerializedName("melancholic")
    MELANCHOLIC,

    @SerializedName("phlegmatic")
    PHLEGMATIC,

    @SerializedName("sanguine")
    SANGUINE,

    @SerializedName("choleric")
    CHOLERIC
}

enum class RequestStatus {
    IN_PROGRESS,
    SUCCESS,
    ERROR
}