package task.kontur.contacts.domain

object TextUtils {
    private const val EMPTY = ""

    fun simplifyPhoneNumber(phoneNumber: String): String =
        phoneNumber
            .replace("+", EMPTY)
            .replace("-", EMPTY)
            .replace(" ", EMPTY)
            .replace("(", EMPTY)
            .replace(")", EMPTY)
}