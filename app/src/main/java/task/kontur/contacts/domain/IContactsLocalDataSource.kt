package task.kontur.contacts.domain

import io.reactivex.Flowable

interface IContactsLocalDataSource {
    fun getContactFlow(id: String): Flowable<Contact>
    fun getContactsList(filter: String = ""): Flowable<List<Contact>>

    fun storeContactsListPortion(portion: List<Contact>)

    fun storeLastUpdateTimeFlow(time: Long)
    fun getLastUpdateTime(defaultTimestamp: Long): Long
    fun getContactsByPhone(phoneNumberFilter: String): Flowable<List<Contact>>
}