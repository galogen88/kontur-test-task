package task.kontur.contacts.domain

import io.reactivex.Flowable

interface IContactsUseCases {
    fun getContactsListFlow(): Flowable<List<Contact>>
    fun setFilter(filter: String)

    fun loadingStatus(): Flowable<RequestStatus>
    fun updateContactsList()

    fun getContactInfo(id: String): Flowable<Contact>
}