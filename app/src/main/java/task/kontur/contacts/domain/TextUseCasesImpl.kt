package task.kontur.contacts.domain

import android.content.Context
import android.os.Build
import android.telephony.PhoneNumberUtils
import android.text.format.DateFormat
import task.kontur.R
import java.util.*
import javax.inject.Inject

class TextUseCasesImpl @Inject constructor(
    private val context: Context
) : ITextUseCases {
    companion object {
        private const val RUSSIA_PREFIX = "7"
        private const val RUSSIA_PHONE_LENGTH = 11
    }

    override fun localizePeriod(period: Period): String {
        val dateFormat = DateFormat.getDateFormat(context)
        return "${dateFormat.format(period.start)} - ${dateFormat.format(period.end)}"
    }

    override fun localizeTemperament(temperament: Temperament): String {
        val stringResId = when (temperament) {
            Temperament.MELANCHOLIC -> R.string.melancholic
            Temperament.PHLEGMATIC -> R.string.phlegmatic
            Temperament.SANGUINE -> R.string.sanguine
            Temperament.CHOLERIC -> R.string.choleric
        }
        return context.getString(stringResId)
    }

    override fun formatSimplePhoneNumber(phone: String): String {
        return when {
            isValidRuPhone(phone) -> formatPhoneNumberRU(phone)
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ->
                PhoneNumberUtils.formatNumber(phone, Locale.getDefault().country)
            else -> PhoneNumberUtils.formatNumber(phone)
        }
    }

    private fun formatPhoneNumberRU(phone: String): String {
        return "+${phone[0]} (${phone.substring(1, 4)}) " +
                "${phone.substring(4, 7)}-${phone.substring(7)}"
    }

    private fun isValidRuPhone(phone: String): Boolean {
        return phone.startsWith(RUSSIA_PREFIX)
                && RUSSIA_PHONE_LENGTH == phone.length
    }

}