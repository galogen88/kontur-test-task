package task.kontur.contacts.domain

import io.reactivex.Single

interface IContactsServerDataSource {
    fun loadPortion(id: String): Single<List<Contact>>
}