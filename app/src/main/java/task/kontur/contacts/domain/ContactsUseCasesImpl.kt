package task.kontur.contacts.domain

import io.reactivex.Flowable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import task.kontur.contacts.presentation.IContactsRepository
import javax.inject.Inject

class ContactsUseCasesImpl @Inject constructor(
    private val repository: IContactsRepository
) : IContactsUseCases {
    companion object {
        const val UPDATE_PERIOD = 1000 * 60
        const val EMPTY_FILTER = ""
        const val BUFFER_SIZE = 300
    }

    private var requestsDisposable = Disposables.disposed()

    init {
        repository.storeFilter(EMPTY_FILTER)
        val lastUpdateTimestamp = repository.getTimestamp(0L)
        if (System.currentTimeMillis() - lastUpdateTimestamp > UPDATE_PERIOD) {
            updateContactsList()
        } else {
            repository.storeRequestStatus(RequestStatus.SUCCESS)
        }
    }

    override fun getContactInfo(id: String): Flowable<Contact> {
        return repository.getContactFlow(id)
            .distinctUntilChanged()
    }

    override fun updateContactsList() {
        if (requestsDisposable.isDisposed) {
            repository.storeRequestStatus(RequestStatus.IN_PROGRESS)
            requestsDisposable = repository.requestContacts()
                .subscribeOn(Schedulers.io())
                .flatMap { Flowable.fromIterable(it) }
                .map { rawContact ->
                    rawContact.copy(
                        phone = TextUtils.simplifyPhoneNumber(rawContact.phone)
                    )
                }
                .buffer(BUFFER_SIZE)
                .subscribe(
                    { repository.storeContacts(it) },
                    { repository.storeRequestStatus(RequestStatus.ERROR) },
                    {
                        repository.storeRequestStatus(RequestStatus.SUCCESS)
                        repository.storeTimestamp(System.currentTimeMillis())
                    }
                )
        }
    }

    override fun getContactsListFlow(): Flowable<List<Contact>> {
        return repository.getFilterFlow()
            .map { it.trim() }
            .distinctUntilChanged()
            .switchMap { filter ->
                repository.getContactsFlow(filter)
                    .flatMap {
                        if (it.isEmpty()) {
                            // format user's filter to search by phone number
                            repository.getContactsByPhoneNumber(TextUtils.simplifyPhoneNumber(filter))
                        } else {
                            Flowable.just(it)
                        }
                    }
            }
    }

    override fun setFilter(filter: String) {
        repository.storeFilter(filter)
    }

    override fun loadingStatus(): Flowable<RequestStatus> = repository.getRequestStatus()
}