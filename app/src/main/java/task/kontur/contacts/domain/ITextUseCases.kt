package task.kontur.contacts.domain

interface ITextUseCases {
    fun localizePeriod(period: Period): String
    fun localizeTemperament(temperament: Temperament): String
    fun formatSimplePhoneNumber(phone: String): String
}