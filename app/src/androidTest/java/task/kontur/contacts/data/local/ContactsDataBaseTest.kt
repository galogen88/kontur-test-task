package task.kontur.contacts.data.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import task.kontur.contacts.domain.Contact
import task.kontur.contacts.domain.Period
import task.kontur.contacts.domain.Temperament
import java.io.IOException
import java.util.*

@RunWith(AndroidJUnit4::class)
class ContactsDataBaseTest {
    private lateinit var contactsDao: ContactsDao
    private lateinit var dataBase: ContactsDataBase

    @Before
    fun createDataBase() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        dataBase = Room.inMemoryDatabaseBuilder(
            context,
            ContactsDataBase::class.java
        ).build()
        contactsDao = dataBase.contactsDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        dataBase.close()
    }

    @Test
    fun writeContactsAndReadInList() {
        contactsDao.insertContactsList(
            mockContactsList(3)
        )
        assertEquals(3, contactsDao.getFilteredContactsList("%%").blockingFirst().size)
        assertEquals(0, contactsDao.getFilteredContactsList("").blockingFirst().size)
    }

    @Test
    fun simpleFilterTest() {
        val ironMan = mockContact(
            id = "111",
            name = "Iron Man",
            phone = "111111"
        )
        contactsDao.insertContactsList(
            listOf(
                ironMan,
                mockContact(
                    id = "222",
                    name = "AAAA bbb",
                    phone = "2222222"
                ),
                mockContact(
                    id = "333",
                    name = "CCC DDDD222",
                    phone = "333333"
                )
            )
        )
        assertEquals(listOf(ironMan), contactsDao.getFilteredContactsList("%iron%").blockingFirst())
        assertEquals(listOf(ironMan), contactsDao.getFilteredContactsList("%n m%").blockingFirst())
        assertEquals(2, contactsDao.getFilteredContactsList("%222%").blockingFirst().size)
    }

    @Test
    fun phoneNumberSearchTest() {
        val contact = mockContact(
            id = "111",
            phone = "79221234567"
        )
        contactsDao.insertContactsList(
            listOf(
                contact,
                mockContact(
                    id = "222",
                    phone = "712345667890"
                )
            )

        )
        assertEquals(
            listOf(contact),
            contactsDao.getContactsListByPhoneNumber("%7922%").blockingFirst()
        )
    }

    private fun mockContactsList(count: Int): List<Contact> {
        val list = mutableListOf<Contact>()
        for (i in 1..count) {
            list.add(mockContact(id = i.toString()))
        }
        return list
    }

    private fun mockContact(
        id: String = "123",
        name: String = "Iron Man",
        phone: String = "7 (922) 123 45 56",
        height: Float = 123f,
        biography: String = "Bla bla bla",
        temperament: Temperament = Temperament.CHOLERIC,
        educationPeriod: Period = Period(
            Date(123),
            Date(321)
        )
    ): Contact {
        return Contact(
            id,
            name,
            phone,
            height,
            biography,
            temperament,
            educationPeriod
        )
    }
}