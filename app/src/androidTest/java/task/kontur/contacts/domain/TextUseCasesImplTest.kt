package task.kontur.contacts.domain

import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TextUseCasesImplTest {

    lateinit var textUseCasesImpl: TextUseCasesImpl

    @Before
    fun setup() {
        textUseCasesImpl = TextUseCasesImpl(
            InstrumentationRegistry.getInstrumentation().context
        )
    }

    @Test
    fun formatPhoneNumber() {
        assertEquals("+7 (922) 123-4567", textUseCasesImpl.formatSimplePhoneNumber("79221234567"))
        assertEquals("(123) 456-7878", textUseCasesImpl.formatSimplePhoneNumber("1234567878"))
    }
}