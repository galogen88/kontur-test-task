package task.kontur.contacts.domain

import org.junit.Test

import org.junit.Assert.*

class TextUtilsTest {

    @Test
    fun simplifyPhoneNumber() {
        assertEquals("79221234567", TextUtils.simplifyPhoneNumber("+7 (922) 123-4567"))
    }
}